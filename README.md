# To build image

```
docker build -t m2m.router .
```

# To run standalone

```
docker run -p 1972:1972 -e MODLOG_DNS_NAME=localhost -e LOBSTERS_DNS_NAME=localhost:3000 m2m.router
```

But this works best as part of the Lobsters docker-compose.yml
